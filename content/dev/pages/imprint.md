# Imprint

## Angabe gemäss § 5 TMG

Christian Rothe  
Nedderfeld 110d
22529 Hamburg

## Kontakt:
E-Mail: christian.rothe.dev at gmail dot com

## Streitschlichtung
Die Europäische Kommission stellt eine Plattform zur Online-Streitbeilegung (OS) bereit: [https://ec.europa.eu/consumers/odr](https://ec.europa.eu/consumers/odr).
Unsere E-Mail-Adresse finden Sie oben im Impressum.

Wir sind nicht bereit oder verpflichtet, an Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle teilzunehmen.
