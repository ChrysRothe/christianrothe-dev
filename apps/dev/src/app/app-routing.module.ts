import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { Routes } from '@christianrothe-dev/core/data-access/router';

const routes: Routes = [
  {
    path: 'about',
    loadChildren: () =>
      import('@christianrothe-dev/blog/feature/about').then(
        (m) => m.BlogFeatureAboutModule
      ),
    data: {
      title: 'About Me',
    },
  },
  {
    path: 'contact',
    loadChildren: () =>
      import('@christianrothe-dev/blog/feature/contact').then(
        (m) => m.BlogFeatureContactModule
      ),
    data: {
      title: 'Contact',
    },
  },
  {
    path: '',
    loadChildren: () =>
      import('@christianrothe-dev/blog/feature/home').then(
        (m) => m.BlogFeatureHomeModule
      ),
    data: {
      title: 'Home',
    },
  },
  {
    path: 'resources',
    loadChildren: () =>
      import('@christianrothe-dev/blog/feature/resources').then(
        (m) => m.BlogFeatureResourcesModule
      ),
  },
  {
    path: 'legal',
    loadChildren: () =>
      import('@christianrothe-dev/blog/feature/legal').then(
        (m) => m.BlogFeatureLegalModule
      ),
  },
  {
    path: '**',
    redirectTo: '',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
