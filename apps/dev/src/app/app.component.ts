import { Component } from '@angular/core';
import { ScrollPositionRestorationService } from '@christianrothe-dev/core/data-access/router';

@Component({
  selector: 'christianrothe-dev-root',
  template: `
    <ui-navbar></ui-navbar>
    <main>
      <router-outlet></router-outlet>
    </main>
    <ui-footer></ui-footer>
  `,
  styles: [
    `
      main {
        min-height: 100vh;
      }
    `,
  ],
})
export class AppComponent {
  constructor(
    private scrollPositionRestorationService: ScrollPositionRestorationService
  ) {
    scrollPositionRestorationService.scrollRestoration$.subscribe();
  }
}
