import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import {
  FooterComponentModule,
  NavbarComponentModule,
} from '@christianrothe-dev/blog/ui/layout';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { EffectsModule } from '@ngrx/effects';

import { TransferHttpCacheModule } from '@nguniversal/common';
import { CoreDataAccessRouterModule } from '@christianrothe-dev/core/data-access/router';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    TransferHttpCacheModule,
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production,
    }),
    EffectsModule.forRoot([]),
    CoreDataAccessRouterModule.forRoot({
      defaultTitle: 'Christian Rothe',
    }),
    AppRoutingModule,
    FooterComponentModule,
    NavbarComponentModule,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
