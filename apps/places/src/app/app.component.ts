import { Component } from '@angular/core';

@Component({
  selector: 'christianrothe-dev-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'places';
}
