import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { Routes } from '@christianrothe-dev/core/data-access/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('@christianrothe-dev/map/feature/map').then(
        (m) => m.MapFeatureMapModule
      ),
  },
  {
    path: '**',
    redirectTo: '',
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
