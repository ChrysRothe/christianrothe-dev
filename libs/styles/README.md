# styles

This folder contains global styles to be used all over the repository

## How to use it

`@import '~@mod/styles`

or

`@import '~@mod/styles/network`
