import { Route as AngularRoute } from '@angular/router';

export interface RouteData {
  title?: string;
}

export interface Route extends AngularRoute {
  data?: RouteData;
}

export type Routes = Route[];
