import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { CustomRouteSerializer } from './+state/serializers/custom-route.serializer';
import { EffectsModule } from '@ngrx/effects';
import {
  ROUTER_DEFAULT_TITLE,
  RouteTitleEffect,
} from './+state/effects/route-title.effect';
import { StoreModule } from '@ngrx/store';
import { reducers } from './+state/reducers/route.reducer';

export interface RouterModuleOptions {
  defaultTitle: string;
}

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forRoot(reducers, {}),
    EffectsModule.forFeature([RouteTitleEffect]),
    StoreRouterConnectingModule.forRoot({
      serializer: CustomRouteSerializer,
    }),
  ],
})
export class CoreDataAccessRouterModule {
  static forRoot(
    options: RouterModuleOptions
  ): ModuleWithProviders<CoreDataAccessRouterModule> {
    return {
      ngModule: CoreDataAccessRouterModule,
      providers: [
        {
          provide: ROUTER_DEFAULT_TITLE,
          useValue: options.defaultTitle,
        },
      ],
    };
  }
}
