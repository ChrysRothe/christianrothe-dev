import { Params } from '@angular/router';
import { RouteData } from '../../models/route-data.model';

export interface RouterStateUrl {
  url: string;
  params: Params;
  queryParams: Params;
  data: RouteData;
}
