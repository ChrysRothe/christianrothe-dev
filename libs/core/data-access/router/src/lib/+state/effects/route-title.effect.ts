import { Inject, Injectable, InjectionToken, Optional } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { tap } from 'rxjs';
import { Title } from '@angular/platform-browser';

import { ROUTER_NAVIGATION, RouterNavigationAction } from '@ngrx/router-store';
import { RouterStateUrl } from '../models/router-state-url.model';

export const ROUTER_DEFAULT_TITLE = new InjectionToken<string>(
  'ROUTER_DEFAULT_TITLE'
);

@Injectable()
export class RouteTitleEffect {
  constructor(
    private actions$: Actions,
    private title: Title,
    @Optional()
    @Inject(ROUTER_DEFAULT_TITLE)
    private defaultTitle: string | null
  ) {}

  updateTitle$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(ROUTER_NAVIGATION),
        tap((action: RouterNavigationAction<RouterStateUrl>) => {
          if (!this.defaultTitle) {
            return;
          }

          this.title.setTitle(
            action.payload.routerState?.data?.title || this.defaultTitle
          );
        })
      );
    },
    { dispatch: false }
  );
}
