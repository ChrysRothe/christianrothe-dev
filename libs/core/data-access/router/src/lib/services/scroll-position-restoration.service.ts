import { Injectable } from '@angular/core';
import { filter, pairwise, tap } from 'rxjs';
import { Event, Router, Scroll } from '@angular/router';
import { ViewportScroller } from '@angular/common';

@Injectable({
  providedIn: 'root',
})
export class ScrollPositionRestorationService {
  public scrollRestoration$ = this.router.events.pipe(
    filter((event: Event): event is Scroll => event instanceof Scroll),
    pairwise(),
    tap((scrollPair: Scroll[]) => this.handleScrollRestoration(scrollPair))
  );

  constructor(
    private router: Router,
    private viewportScroller: ViewportScroller
  ) {}

  private handleScrollRestoration(scrollPair: Scroll[]): void {
    const previousEvent = scrollPair[0];
    const event = scrollPair[1];

    if (event.position) {
      // backward navigation
      this.viewportScroller.scrollToPosition(event.position);
      return;
    }

    if (event.anchor) {
      // anchor navigation
      this.viewportScroller.scrollToAnchor(event.anchor);
      return;
    }

    if (
      previousEvent.routerEvent.urlAfterRedirects.split('?')[0] !==
      event.routerEvent.urlAfterRedirects.split('?')[0]
    ) {
      // Routes don't match, this is actual forward navigation
      // Default behavior: scroll to top
      this.viewportScroller.scrollToPosition([0, 0]);
    }
  }
}
