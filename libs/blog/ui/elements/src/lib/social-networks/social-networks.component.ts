import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  Input,
  NgModule,
} from '@angular/core';
import { CommonModule } from '@angular/common';

export enum IconSize {
  Default = 'default',
  Small = 'small',
}

@Component({
  selector: 'ui-social-networks',
  template: `
    <span class="icon">
      <a
        href="https://www.linkedin.com/in/christian-rothe-dev"
        target="_blank"
        rel="noopener"
      >
        <i class="fab fa-linkedin"></i>
      </a>
    </span>
    <span class="icon">
      <a href="https://gitlab.com/ChrysRothe" target="_blank" rel="noopener">
        <i class="fab fa-gitlab"></i>
      </a>
    </span>
    <span class="icon">
      <a href="https://github.com/chrysrothe" target="_blank" rel="noopener">
        <i class="fab fa-github-square"></i>
      </a>
    </span>
  `,
  styles: [
    `
      @import '~@christianrothe-dev/styles/colors';

      :host {
        display: flex;
      }

      .icon {
        padding: 1rem;
        font-size: 2rem;
        transition: transform 150ms ease-out, color 350ms ease-out;

        &:hover {
          transform: translateY(-1px);
        }
      }

      a {
        color: $color-text-secondary;

        &:hover {
          transform: translateY(-1px);
          color: $color-text-primary;
        }
      }

      :host.small .icon {
        font-size: 1.3rem;
        padding: 0.5rem;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SocialNetworksComponent {
  @HostBinding('class')
  @Input()
  public size: IconSize = IconSize.Default;
}

@NgModule({
  imports: [CommonModule],
  declarations: [SocialNetworksComponent],
  exports: [SocialNetworksComponent],
})
export class SocialNetworksComponentModule {}
