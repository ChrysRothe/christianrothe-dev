import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  Input,
  NgModule,
} from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'ui-alert',
  template: ` <ng-content></ng-content> `,
  styles: [
    `
      @import '~@christianrothe-dev/styles/colors';

      :host {
        display: block;
        background-color: $color-text-secondary;
        padding: 1.25rem;
        border-radius: 5px;
        font-size: 1.2rem;
        color: #fff;
      }

      :host.success {
        background-color: $color-accent;
      }

      :host.error {
        background-color: $color-error;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AlertComponent {
  @HostBinding('class')
  @Input()
  type: 'success' | 'error' | 'neutral' = 'neutral';
}

@NgModule({
  imports: [CommonModule],
  declarations: [AlertComponent],
  exports: [AlertComponent],
})
export class AlertComponentModule {}
