import { Component, NgModule, ChangeDetectionStrategy } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'h1[uiH1]',
  template: ` <ng-content></ng-content> `,
  styles: [
    `
      @import '~@christianrothe-dev/styles/colors';

      :host {
        font-weight: 700;
        color: $color-text-primary;
        margin-bottom: 0.5rem;
        font-size: 2rem;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class H1Component {}

@NgModule({
  imports: [CommonModule],
  declarations: [H1Component],
  exports: [H1Component],
})
export class H1ComponentModule {}
