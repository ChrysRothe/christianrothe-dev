import { Component, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'ui-sub-title',
  template: ` <ng-content></ng-content> `,
  styles: [
    `
      @import '~@christianrothe-dev/styles/colors';

      :host {
        display: inline-block;
        font-size: 1.2rem;
        line-height: 1.6rem;
        color: $color-text-secondary;
      }
    `,
  ],
})
export class SubTitleComponent {}

@NgModule({
  imports: [CommonModule],
  declarations: [SubTitleComponent],
  exports: [SubTitleComponent],
})
export class SubTitleComponentModule {}
