import { Component, NgModule, ChangeDetectionStrategy } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'h2[uiH2]',
  template: ` <ng-content></ng-content> `,
  styles: [
    `
      @import '~@christianrothe-dev/styles/colors';

      :host {
        font-weight: 700;
        color: $color-text-primary;
        margin-bottom: 0.5rem;
        font-size: 1.8rem;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class H2Component {}

@NgModule({
  imports: [CommonModule],
  declarations: [H2Component],
  exports: [H2Component],
})
export class H2ComponentModule {}
