import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  Input,
  NgModule,
} from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'button[uiButton], a[uiButton]',
  template: ` <ng-content></ng-content> `,
  styles: [
    `
      @import '~@christianrothe-dev/styles/colors';

      :host {
        border-width: 1px;
        cursor: pointer;
        justify-content: center;
        padding: calc(0.5rem - 1px) 1rem;
        text-align: center;
        white-space: nowrap;

        background-color: $color-accent;
        border-color: transparent;
        color: #fff;

        align-items: center;
        border-radius: 0.375em;
        box-shadow: none;
        display: inline-flex;
        font-size: 1rem;
        position: relative;

        text-decoration: none;

        &:hover {
          background-color: $color-accent-dark;
        }

        &[disabled] {
          background-color: $color-accent-light;
          box-shadow: none;
          cursor: not-allowed;
        }
      }

      :host-context(.large) {
        padding: calc(1rem - 1px) 2rem;
        font-size: 1.3rem;
      }

      :host.is-pending {
        color: transparent;
        pointer-events: none;

        &:after {
          animation: spin 0.5s infinite linear;
          border: 2px solid #dbdbdb;
          border-radius: 9999px;
          border-right-color: transparent;
          border-top-color: transparent;
          content: '';
          display: block;
          height: 1em;
          width: 1em;

          left: calc(50% - (1em * 0.5));
          top: calc(50% - (1em * 0.5));
          position: absolute;
        }
      }

      @-webkit-keyframes spin {
        0% {
          -webkit-transform: rotate(0deg);
        }
        100% {
          -webkit-transform: rotate(360deg);
        }
      }

      @keyframes spin {
        0% {
          transform: rotate(0deg);
        }
        100% {
          transform: rotate(360deg);
        }
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ButtonComponent {
  @HostBinding('class.is-pending')
  @Input()
  public isPending: boolean | undefined | null;
}

@NgModule({
  imports: [CommonModule],
  declarations: [ButtonComponent],
  exports: [ButtonComponent],
})
export class ButtonComponentModule {}
