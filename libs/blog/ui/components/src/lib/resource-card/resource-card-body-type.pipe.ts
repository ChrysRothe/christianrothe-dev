import { Pipe, PipeTransform } from '@angular/core';
import { ResourceTypeEnum } from '@christianrothe-dev/blog/util/resources';
import { CardBodyTypeEnum } from '@christianrothe-dev/blog/ui/components';

const mapping: Record<ResourceTypeEnum, CardBodyTypeEnum> = {
  [ResourceTypeEnum.Podcast]: CardBodyTypeEnum.Danger,
  [ResourceTypeEnum.Video]: CardBodyTypeEnum.Orchid,
  [ResourceTypeEnum.Library]: CardBodyTypeEnum.Amber,
  [ResourceTypeEnum.Book]: CardBodyTypeEnum.Ocean,
  [ResourceTypeEnum.Article]: CardBodyTypeEnum.Positive,
};

@Pipe({ name: 'resourceCardBodyType' })
export class ResourceCardBodyTypePipe implements PipeTransform {
  transform(value: ResourceTypeEnum): CardBodyTypeEnum {
    return mapping[value];
  }
}
