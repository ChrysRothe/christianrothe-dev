import {
  Component,
  NgModule,
  ChangeDetectionStrategy,
  Input,
} from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  Resource,
  ResourceIconPipeModule,
} from '@christianrothe-dev/blog/util/resources';
import { ResourceCardBodyTypePipe } from './resource-card-body-type.pipe';
import { CardComponentModule } from '../card/card/card.component';
import { CardBodyComponentModule } from '../card/card-body/card-body.component';
import { CardFooterComponentModule } from '../card/card-footer/card-footer.component';

@Component({
  selector: 'ui-resource-card',
  template: `
    <ui-card *ngIf="resource">
      <ui-card-body [type]="resource.type | resourceCardBodyType">
        <span [title]="resource.type">{{ resource.type | resourceIcon }}</span>
        <a target="_blank" rel="noopener" [href]="resource.url">
          <h2>{{ resource.name }}</h2>
        </a>
      </ui-card-body>
      <ui-card-footer>
        <a target="_blank" rel="noopener" [href]="resource.url"> view </a>
      </ui-card-footer>
    </ui-card>
  `,
  styles: [
    `
      ui-card-body {
        text-align: center;
      }

      ui-card-footer {
        text-align: center;
      }

      h2 {
        font-size: 1.3rem;
        font-weight: 500;
        text-align: center;
        margin: 0;
      }

      span {
        display: inline-block;
        margin-bottom: 0.5rem;
        font-size: 1.4rem;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResourceCardComponent {
  @Input() public resource?: Resource;
}

@NgModule({
  imports: [
    CommonModule,
    CardComponentModule,
    CardBodyComponentModule,
    CardFooterComponentModule,
    ResourceIconPipeModule,
  ],
  declarations: [ResourceCardComponent, ResourceCardBodyTypePipe],
  exports: [ResourceCardComponent],
})
export class ResourceCardComponentModule {}
