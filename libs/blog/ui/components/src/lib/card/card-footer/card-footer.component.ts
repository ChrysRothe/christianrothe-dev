import { Component, NgModule, ChangeDetectionStrategy } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'ui-card-footer',
  template: ` <ng-content></ng-content> `,
  styles: [
    `
      :host {
        padding: 1rem;
        display: block;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CardFooterComponent {}

@NgModule({
  imports: [CommonModule],
  declarations: [CardFooterComponent],
  exports: [CardFooterComponent],
})
export class CardFooterComponentModule {}
