import { ChangeDetectionStrategy, Component, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'ui-card',
  template: `
    <ng-content select="ui-card-body"></ng-content>
    <ng-content select="ui-card-footer"></ng-content>
  `,
  styles: [
    `
      :host {
        display: flex;
        flex-direction: column;
        height: 100%;
        width: 100%;
        border-radius: 5px;
        box-shadow: 0 7px 11px -4px rgb(0 23 62 / 20%), 0 0 1px 0 #a8b9d5;

        transition: transform 150ms ease-out, color 350ms ease-out;

        &:hover {
          transform: translateY(-2px);
        }
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CardComponent {}

@NgModule({
  imports: [CommonModule],
  declarations: [CardComponent],
  exports: [CardComponent],
})
export class CardComponentModule {}
