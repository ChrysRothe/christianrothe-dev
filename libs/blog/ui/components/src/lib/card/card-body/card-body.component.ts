import {
  Component,
  NgModule,
  ChangeDetectionStrategy,
  HostBinding,
  Input,
} from '@angular/core';
import { CommonModule } from '@angular/common';

export enum CardBodyTypeEnum {
  Ocean = 'ocean',
  Positive = 'positive',
  Amber = 'amber',
  Danger = 'danger',
  Orchid = 'orchid',
  None = 'none',
}

@Component({
  selector: 'ui-card-body',
  template: ` <ng-content></ng-content> `,
  styles: [
    `
      :host {
        padding: 1rem;
        flex-grow: 1;
      }

      :host.ocean {
        background-color: #f5f8ff;
        color: #002ca6;
      }

      :host.positive {
        background-color: #e0ffef;
        color: #006631;
      }

      :host.amber {
        background-color: #fff7e5;
        color: #996b00;
      }

      :host.danger {
        background-color: #ffeff5;
        color: #d82a68;
      }

      :host.orchid {
        background-color: #fdf1ff;
        color: #740089;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CardBodyComponent {
  @HostBinding('class')
  @Input()
  type: CardBodyTypeEnum = CardBodyTypeEnum.None;
}

@NgModule({
  imports: [CommonModule],
  declarations: [CardBodyComponent],
  exports: [CardBodyComponent],
})
export class CardBodyComponentModule {}
