import { Component, OnInit, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'textarea[uiFormsTextarea]',
  template: ` <ng-content></ng-content>`,
  styles: [
    `
      :host {
        padding: calc(0.75em - 1px);
        resize: vertical;

        &:not([rows]) {
          max-height: 40em;
          min-height: 8em;
        }
      }
    `,
  ],
  styleUrls: ['../form-styles.scss'],
})
export class TextareaComponent {}

@NgModule({
  imports: [CommonModule],
  declarations: [TextareaComponent],
  exports: [TextareaComponent],
})
export class TextareaComponentModule {}
