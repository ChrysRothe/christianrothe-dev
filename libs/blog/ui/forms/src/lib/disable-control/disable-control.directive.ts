import { NgModule, Directive, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[uiFormsDisableControl]',
})
export class DisableControlDirective {
  @Input() set uiFormsDisableControl(condition: boolean | null) {
    const action = condition ? 'disable' : 'enable';
    this.ngControl.control?.[action]();
  }

  constructor(private ngControl: NgControl) {}
}

@NgModule({
  imports: [CommonModule],
  declarations: [DisableControlDirective],
  exports: [DisableControlDirective],
})
export class DisableControlDirectiveModule {}
