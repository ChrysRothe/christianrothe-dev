import { Component, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'input[uiFormsInput]',
  template: ` <ng-content></ng-content> `,
  styles: [
    `
      :host {
        padding: calc(0.5em - 1px) calc(0.5em - 1px);
      }
    `,
  ],
  styleUrls: ['../form-styles.scss'],
})
export class InputComponent {}

@NgModule({
  imports: [CommonModule],
  declarations: [InputComponent],
  exports: [InputComponent],
})
export class InputComponentModule {}
