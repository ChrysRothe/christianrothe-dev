import { Component, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'select[uiFormsSelect]',
  template: ` <ng-content></ng-content> `,
  styles: [
    `
      :host {
        padding: calc(0.5em - 1px) calc(0.5em - 1px);
      }
    `,
  ],
  styleUrls: ['../form-styles.scss'],
})
export class SelectComponent {}

@NgModule({
  imports: [CommonModule],
  declarations: [SelectComponent],
  exports: [SelectComponent],
})
export class SelectComponentModule {}
