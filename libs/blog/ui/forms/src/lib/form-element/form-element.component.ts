import {
  Component,
  OnInit,
  NgModule,
  Input,
  ContentChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  AbstractControl,
  FormControlName,
  FormGroup,
  FormGroupDirective,
} from '@angular/forms';

@Component({
  selector: 'form-element',
  template: `
    <label>
      <span *ngIf="label">{{ label }}</span>
      <ng-content></ng-content>
    </label>

    <p *ngIf="formControl && formControl.errors && formControl.touched">
      {{ formControl.errors | json }}
    </p>
  `,
  styles: [
    `
      @import '~@christianrothe-dev/styles/colors';

      :host {
        display: block;
        margin-bottom: 1rem;
      }

      label {
        display: block;
        margin-bottom: 0.25rem;
      }

      span {
        display: inline-block;
        margin-bottom: 0.25rem;
      }

      p {
        color: $color-error;
      }
    `,
  ],
  providers: [FormGroupDirective],
})
export class FormElementComponent implements OnInit {
  constructor(public formControlDirective: FormGroupDirective) {}
  @Input() public label: string | undefined;

  @ContentChild(FormControlName, { static: true })
  public controlName: FormControlName | undefined;

  public formControl: AbstractControl | null | undefined;
  public formGroup: FormGroup | undefined;

  public ngOnInit(): void {
    this.formGroup = this.formControlDirective.form;
    this.formControl = this.formGroup?.get(this.controlName?.name as string);
  }
}

@NgModule({
  imports: [CommonModule],
  declarations: [FormElementComponent],
  exports: [FormElementComponent],
})
export class FormElementComponentModule {}
