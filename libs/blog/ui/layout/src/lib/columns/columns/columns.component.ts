import { ChangeDetectionStrategy, Component, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'ui-columns',
  template: ` <ng-content></ng-content> `,
  styles: [
    `
      @import '~@christianrothe-dev/styles/break_points';

      :host {
        display: block;
        margin: -0.75rem;

        @include tablet {
          display: flex;
          flex-wrap: wrap;
        }
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ColumnsComponent {}

@NgModule({
  imports: [CommonModule],
  declarations: [ColumnsComponent],
  exports: [ColumnsComponent],
})
export class ColumnsComponentModule {}
