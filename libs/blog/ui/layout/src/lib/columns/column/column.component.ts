import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  Input,
  NgModule,
} from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'ui-column',
  template: ` <ng-content></ng-content> `,
  styles: [
    `
      @import '~@christianrothe-dev/styles/break_points';

      :host {
        display: block;
        flex-grow: 1;
        padding: 0.75rem;
      }

      @include tablet {
        :host.is-half {
          width: 50%;
          flex-grow: 0;
        }

        :host.is-third {
          width: 33%;
          flex-grow: 0;
        }

        :host.is-two-third {
          width: 66%;
          flex-grow: 0;
        }

        :host.is-fourth {
          width: 25%;
          flex-grow: 0;
        }
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ColumnComponent {
  @HostBinding('class')
  @Input()
  size: 'is-half' | 'is-third' | 'is-two-third' | 'is-fourth' | 'none' = 'none';
}

@NgModule({
  imports: [CommonModule],
  declarations: [ColumnComponent],
  exports: [ColumnComponent],
})
export class ColumnComponentModule {}
