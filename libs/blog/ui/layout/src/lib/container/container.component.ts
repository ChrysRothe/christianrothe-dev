import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  Input,
  NgModule,
} from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'ui-container',
  template: ` <ng-content></ng-content> `,
  styles: [
    `
      @import '~@christianrothe-dev/styles/break_points';

      :host {
        display: block;
        padding: 0 0.75rem;

        margin: 0 auto;

        @include full-dh {
          max-width: 1344px;
        }
      }

      :host.center {
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContainerComponent {
  @HostBinding('class')
  @Input()
  public alignment: 'left' | 'center' = 'left';
}

@NgModule({
  imports: [CommonModule],
  declarations: [ContainerComponent],
  exports: [ContainerComponent],
})
export class ContainerComponentModule {}
