import { ChangeDetectionStrategy, Component, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'section[ui-section]',
  template: ` <ng-content></ng-content> `,
  styles: [
    `
      @import '~@christianrothe-dev/styles/break_points';

      :host {
        padding: 2rem 0;
        display: block;

        @include desktop {
          padding: 4rem 0;
        }

        @include full-dh {
          padding: 6rem 0;
        }
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SectionComponent {}

@NgModule({
  imports: [CommonModule],
  declarations: [SectionComponent],
  exports: [SectionComponent],
})
export class SectionComponentModule {}
