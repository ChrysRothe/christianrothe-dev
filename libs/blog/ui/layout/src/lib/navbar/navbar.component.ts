import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  NgModule,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ContainerComponentModule } from '../container/container.component';

@Component({
  selector: 'ui-navbar',
  template: `
    <ui-container>
      <i class="fa-solid fa-circle-info"></i> &nbsp; this site is currently
      under construction
    </ui-container>
    <div class="nav">
      <div class="brand">
        <strong [routerLink]="['']">Christian Rothe</strong>
        <div (click)="isActive = !isActive" class="burger">
          <span></span>
          <span></span>
        </div>
      </div>
      <ul>
        <li (click)="isActive = false">
          <a
            [routerLink]="['/']"
            routerLinkActive="active-link"
            [routerLinkActiveOptions]="{ exact: true }"
          >
            Home</a
          >
        </li>
        <li (click)="isActive = false">
          <a [routerLink]="['resources/angular']" routerLinkActive="active-link"
            >Angular Resources</a
          >
        </li>
        <li (click)="isActive = false">
          <a [routerLink]="['about']" routerLinkActive="active-link"
            >About Me</a
          >
        </li>
      </ul>
    </div>
  `,
  styles: [
    `
      @import '~@christianrothe-dev/styles/colors';
      @import '~@christianrothe-dev/styles/break_points';

      $burger-strike-width: 20px;
      $burger-strike-top: 3px;
      $background: #fff;
      $distance-x: 0.75rem;
      $navbar-height: 50px;

      :host {
        width: 100%;
        position: fixed;
        top: 0;
        background: $background;
        z-index: 10000;
      }

      ui-container {
        display: flex;
        justify-content: center;
        align-items: center;
        border-bottom: 1px solid rgba(0, 0, 0, 0.1);
        height: 30px;
      }

      .nav {
        width: 100%;
        height: $navbar-height;
        margin: 0 auto;

        @include desktop {
          display: flex;
          justify-content: space-between;
          padding: 0 $distance-x;
        }

        @include full-dh {
          max-width: 1344px;
        }
      }

      ul {
        display: none;
        flex-direction: column;
        background: $background;
        flex-shrink: 0;
        padding-left: $distance-x;

        box-shadow: 0 8px 11px -3px rgb(0 23 62 / 20%);

        @include desktop {
          box-shadow: none;
          padding: 0;
          height: 100%;
          display: flex;
          flex-direction: row;
          gap: 3rem;
        }
      }

      :host.is-active ul {
        display: flex;
      }

      strong {
        display: flex;
        align-items: center;
      }

      li {
        height: 40px;
        display: flex;
        align-items: center;
        cursor: pointer;

        @include desktop {
          height: $navbar-height;
        }
      }

      a {
        color: $color-text-secondary;
        display: flex;
        height: 100%;
        align-items: center;

        &:hover {
          color: $color-text-primary;
        }
      }

      a.active-link {
        color: $color-text-primary;
      }

      .brand {
        height: 100%;
        width: 100%;
        display: flex;
        justify-content: space-between;
        padding-left: $distance-x;
        cursor: pointer;

        @include desktop {
          padding-left: 0;
        }
      }

      .burger {
        position: relative;
        width: 50px;
        display: flex;
        justify-content: center;
        align-items: center;

        @include desktop {
          display: none;
        }
      }

      span {
        background-color: $color-text-secondary;
        display: block;
        height: 1px;
        position: absolute;
        width: $burger-strike-width;
        left: calc(50% - (#{$burger-strike-width} / 2));

        transform-origin: center;
        transition-duration: 100ms;
        transition-property: transform;
        transition-timing-function: ease-out;

        &:first-child {
          top: calc(50% - #{$burger-strike-top});
        }

        &:last-child {
          top: calc(50% + #{$burger-strike-top});
        }
      }

      :host.is-active span:first-child {
        transform: translateY($burger-strike-top) rotate(45deg);
      }

      :host.is-active span:last-child {
        transform: translateY(-$burger-strike-top) rotate(-45deg);
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavbarComponent {
  @HostBinding('class.is-active')
  public isActive: boolean = false;
}

@NgModule({
  imports: [CommonModule, RouterModule, ContainerComponentModule],
  declarations: [NavbarComponent],
  exports: [NavbarComponent],
})
export class NavbarComponentModule {}
