import { ChangeDetectionStrategy, Component, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  IconSize,
  SocialNetworksComponentModule,
} from '@christianrothe-dev/blog/ui/elements';
import { ContainerComponentModule } from '../container/container.component';
import { ColumnsComponentModule } from '../columns/columns/columns.component';
import { ColumnComponentModule } from '../columns/column/column.component';
import { SectionComponentModule } from '../section/section.component';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'ui-footer',
  template: ` <section ui-section>
    <ui-container>
      <ui-columns>
        <ui-column [size]="'is-fourth'">
          <span>christianrothe.dev | 2022</span>
        </ui-column>

        <ui-column [size]="'is-fourth'">
          <span class="heading">Links</span>
          <ul>
            <li>
              <a [routerLink]="''"> Home </a>
            </li>
            <li>
              <a [routerLink]="'about'">About</a>
            </li>
            <li>
              <a [routerLink]="'resources/angular'">Angular Resources</a>
            </li>
          </ul>
        </ui-column>

        <ui-column [size]="'is-fourth'">
          <span class="heading">Social</span>
          <ul>
            <li>
              <a
                href="https://www.linkedin.com/in/christian-rothe-dev"
                target="_blank"
                rel="noopener"
              >
                <i class="fab fa-linkedin"></i> LinkedIn
              </a>
            </li>
            <li>
              <a
                href="https://twitter.com/ChrysRothe"
                target="_blank"
                rel="noopener"
              >
                <i class="fab fa-twitter"></i> Twitter
              </a>
            </li>
            <li>
              <a
                href="https://github.com/chrysrothe"
                target="_blank"
                rel="noopener"
              >
                <i class="fab fa-github-square"></i> Github
              </a>
            </li>
            <li>
              <a
                href="https://gitlab.com/ChrysRothe"
                target="_blank"
                rel="noopener"
              >
                <i class="fab fa-gitlab"></i> Gitlab
              </a>
            </li>
          </ul>
        </ui-column>

        <ui-column [size]="'is-fourth'">
          <span class="heading">Legal</span>
          <ul>
            <li>
              <a [routerLink]="['legal', 'imprint']"> Imprint </a>
            </li>
            <li>
              <a [routerLink]="['legal', 'privacy']">Privacy</a>
            </li>
          </ul>
        </ui-column>
      </ui-columns>
    </ui-container>
  </section>`,
  styles: [
    `
      @import '~@christianrothe-dev/styles/colors';

      .heading {
        font-size: 1.2rem;
        margin-bottom: 1rem;
        display: inline-block;
      }

      section {
        padding: 2rem 0;
        border-top: 1px solid rgba(0, 0, 0, 0.1);
        background: $background-color-primary;
      }

      li {
        color: $color-text-secondary;
        margin-bottom: 0.75rem;
        transition: transform 150ms ease-out, color 350ms ease-out;

        &:hover {
          color: $color-text-primary;
          transform: translateY(-1px);
        }
      }

      li:last-child {
        margin-bottom: 0;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FooterComponent {
  public iconSize: IconSize = IconSize.Small;
}

@NgModule({
  imports: [
    CommonModule,
    SocialNetworksComponentModule,
    ContainerComponentModule,
    ColumnsComponentModule,
    ColumnComponentModule,
    SectionComponentModule,
    RouterModule,
  ],
  declarations: [FooterComponent],
  exports: [FooterComponent],
})
export class FooterComponentModule {}
