import { TestBed } from '@angular/core/testing';

import { AngularResourcesClient } from './angular-resources.client';

describe('AngularResourcesService', () => {
	let service: AngularResourcesClient;

	beforeEach(() => {
		TestBed.configureTestingModule({});
		service = TestBed.inject(AngularResourcesClient);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});
});
