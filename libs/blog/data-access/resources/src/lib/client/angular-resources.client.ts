import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Resources } from '@christianrothe-dev/blog/util/resources';

@Injectable()
export class AngularResourcesClient {
  constructor(private http: HttpClient) {}

  public get(): Observable<Resources> {
    return this.http.get<Resources>('api/resources/angular');
  }
}
