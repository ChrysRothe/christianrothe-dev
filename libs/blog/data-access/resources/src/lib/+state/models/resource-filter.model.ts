import { ResourceTypeEnum } from '@christianrothe-dev/blog/util/resources';

export enum ResourceFilterKeyEnum {
  Type = 'type',
  Search = 'search',
}

export type ResourceFilter = {
  [ResourceFilterKeyEnum.Type]: ResourceTypeEnum | 'all';
  [ResourceFilterKeyEnum.Search]: string;
};
