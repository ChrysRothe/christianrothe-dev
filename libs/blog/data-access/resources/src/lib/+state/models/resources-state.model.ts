import { ResourceFilter } from './resource-filter.model';
import { Resources } from '@christianrothe-dev/blog/util/resources';

export const resourcesFeatureKey = 'angularResources';

export interface ResourceState {
  resources: Resources;
  filter: ResourceFilter;
}
