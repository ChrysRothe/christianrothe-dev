import { createFeature, createReducer, on } from '@ngrx/store';

import {
  resourcesFeatureKey,
  ResourceState,
} from '../models/resources-state.model';
import {
  setResourceFilter,
  loadResourcesSuccess,
} from '../actions/resources.actions';

export const initialState: ResourceState = {
  resources: [],
  filter: {
    type: 'all',
    search: '',
  },
};

export const angularResourcesFeature = createFeature({
  name: resourcesFeatureKey,
  reducer: createReducer(
    initialState,
    on(
      loadResourcesSuccess,
      (
        state: ResourceState,
        action: ReturnType<typeof loadResourcesSuccess>
      ) => {
        return {
          ...state,
          resources: action.resources,
        };
      }
    ),
    on(
      setResourceFilter,
      (state: ResourceState, action: ReturnType<typeof setResourceFilter>) => {
        return {
          ...state,
          filter: action.filter,
        };
      }
    )
  ),
});
