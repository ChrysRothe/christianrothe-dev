import { createAction, props } from '@ngrx/store';
import { Resources } from '@christianrothe-dev/blog/util/resources';
import { ResourceFilter } from '../models/resource-filter.model';

export enum ResourceActionEnum {
  LoadResources = '[Angular Resources Page] Load Resources',
  LoadResourcesSuccess = '[Angular Resources API] Load Resources Success',
  SetResourceFilter = '[Angular Resources Page] Set Resource Filter',
  UpdateResourceFilter = '[Angular Resources Page] Update Resources Filter',
}

export const loadResources = createAction(ResourceActionEnum.LoadResources);

export const loadResourcesSuccess = createAction(
  ResourceActionEnum.LoadResourcesSuccess,
  props<{ resources: Resources }>()
);

export const setResourceFilter = createAction(
  ResourceActionEnum.SetResourceFilter,
  props<{ filter: ResourceFilter }>()
);

export const updateResourceFilter = createAction(
  ResourceActionEnum.UpdateResourceFilter,
  props<{ filter: ResourceFilter }>()
);
