import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';

import { EffectsModule } from '@ngrx/effects';

import { ResourcesEffects } from './effects/resources.effects';
import { angularResourcesFeature } from './reducers/resources.reducer';
import { UpdateSearchQueryParamEffects } from './effects/update-search-query-param.effects';
import { ResourceFilterEffects } from './effects/resource-filter.effects';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    EffectsModule.forFeature([
      ResourceFilterEffects,
      ResourcesEffects,
      UpdateSearchQueryParamEffects,
    ]),
    StoreModule.forFeature(angularResourcesFeature),
    EffectsModule.forFeature(),
  ],
})
export class ResourcesStateModule {}
