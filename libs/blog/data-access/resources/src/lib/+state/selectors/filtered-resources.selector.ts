import { createSelector } from '@ngrx/store';

import { Resource, Resources } from '@christianrothe-dev/blog/util/resources';
import { angularResourcesFeature } from '../reducers/resources.reducer';
import { ResourceFilter } from '../models/resource-filter.model';

export const selectFilteredResources = createSelector(
  angularResourcesFeature.selectResources,
  angularResourcesFeature.selectFilter,
  (resources: Resources, filter: ResourceFilter) => {
    const search = filter.search;
    const type = filter.type;

    if (search === '' && type === 'all') {
      return resources;
    }

    const filteredResources = resources
      .slice(0)
      .filter((resource: Resource) => {
        const hasSearchString =
          search !== ''
            ? resource.name.toLocaleLowerCase().includes(search.toLowerCase())
            : true;

        const isType = type !== 'all' ? resource.type === type : true;

        return hasSearchString && isType;
      });

    return filteredResources.length > 0 ? filteredResources : null;
  }
);
