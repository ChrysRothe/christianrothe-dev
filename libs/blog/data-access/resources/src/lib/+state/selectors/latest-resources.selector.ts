import { createSelector } from '@ngrx/store';

import { Resources } from '@christianrothe-dev/blog/util/resources';
import { angularResourcesFeature } from '../reducers/resources.reducer';

export const selectLatestResources = (count: number = 3) =>
  createSelector(
    angularResourcesFeature.selectResources,
    (resources: Resources) => {
      const end = resources.length - 1;
      return count > end ? resources : resources.slice(end - count, end);
    }
  );
