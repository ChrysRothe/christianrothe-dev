import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY, filter, Observable, of, switchMap } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Action } from '@ngrx/store';
import { ResourceActionEnum } from '../actions/resources.actions';
import { ROUTER_NAVIGATION, RouterNavigationAction } from '@ngrx/router-store';
import { RouterStateUrl } from '@christianrothe-dev/core/data-access/router';
import { Params } from '@angular/router';
import { ResourceFilterKeyEnum } from '@christianrothe-dev/blog/data-access/resources';

@Injectable()
export class ResourceFilterEffects {
  constructor(private actions$: Actions) {}

  loadResourceFilter$: Observable<Action> = createEffect(() => {
    return this.actions$.pipe(
      ofType(ROUTER_NAVIGATION),
      filter((action: RouterNavigationAction<RouterStateUrl>) =>
        action.payload.routerState.url.includes('resources/angular')
      ),
      switchMap((action: RouterNavigationAction<RouterStateUrl>) => {
        return of(action.payload.routerState.queryParams).pipe(
          map((params: Params) => ({
            type: ResourceActionEnum.SetResourceFilter,
            filter: {
              type: params[ResourceFilterKeyEnum.Type] || 'all',
              search: params[ResourceFilterKeyEnum.Search] || '',
            },
          })),
          catchError(() => EMPTY)
        );
      })
    );
  });
}
