import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType, OnInitEffects } from '@ngrx/effects';
import { EMPTY, Observable } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { Action } from '@ngrx/store';
import { AngularResourcesClient } from '../../client/angular-resources.client';
import {
  loadResources,
  ResourceActionEnum,
} from '../actions/resources.actions';
import { Resources } from '@christianrothe-dev/blog/util/resources';

@Injectable()
export class ResourcesEffects implements OnInitEffects {
  constructor(
    private actions$: Actions,
    private angularResourcesClient: AngularResourcesClient
  ) {}

  loadResources$: Observable<Action> = createEffect(() => {
    return this.actions$.pipe(
      ofType(loadResources),
      mergeMap(() =>
        this.angularResourcesClient.get().pipe(
          map((resources: Resources) => ({
            type: ResourceActionEnum.LoadResourcesSuccess,
            resources: resources,
          })),
          catchError(() => EMPTY)
        )
      )
    );
  });

  ngrxOnInitEffects(): Action {
    return { type: ResourceActionEnum.LoadResources };
  }
}
