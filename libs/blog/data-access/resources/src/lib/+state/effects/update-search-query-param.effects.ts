import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { tap } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { updateResourceFilter } from '../actions/resources.actions';
import { ResourceFilterKeyEnum } from '../models/resource-filter.model';

@Injectable()
export class UpdateSearchQueryParamEffects {
  constructor(
    private actions$: Actions,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  updateSearchQueryParam$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(updateResourceFilter),
        tap((action) =>
          this.router.navigate([], {
            relativeTo: this.activatedRoute,
            queryParams: {
              [ResourceFilterKeyEnum.Search]: action.filter.search,
              [ResourceFilterKeyEnum.Type]: action.filter.type,
            },
            queryParamsHandling: 'merge',
          })
        )
      );
    },
    { dispatch: false }
  );
}
