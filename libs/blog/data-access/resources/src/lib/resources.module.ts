import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResourcesStateModule } from './+state/resources-state.module';
import { AngularResourcesClient } from './client/angular-resources.client';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [CommonModule, ResourcesStateModule, HttpClientModule],
  providers: [AngularResourcesClient],
})
export class ResourcesModule {}
