import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Contact } from '../models/contact.model';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ContactClient {
  constructor(private http: HttpClient) {}

  public post(contact: Contact): Observable<boolean> {
    return this.http.post<boolean>('api/contact', contact);
  }
}
