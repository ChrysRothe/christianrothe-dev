export interface Contact {
	email: string;
	message: string;
	name: string;
	privacy: boolean;
}
