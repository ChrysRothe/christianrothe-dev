import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { contactFeatureKey } from './models/contact-state.model';
import { contactReducers } from './reducers/contact.reducer';
import { EffectsModule } from '@ngrx/effects';
import { ContactEffects } from './effects/contact.effects';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(contactFeatureKey, contactReducers),
    EffectsModule.forFeature([ContactEffects]),
  ],
})
export class ContactStateModule {}
