import { createAction, props } from '@ngrx/store';
import { Contact } from '../../models/contact.model';

export enum ContactActionEnum {
  ContactRequest = '[Contact API] Contact Request',
  ContactRequestSuccess = '[Contact API] Contact Request Successful',
  ContactRequestFailed = '[Contact API] Contact Request Failed',
}

export const doContactRequest = createAction(
  ContactActionEnum.ContactRequest,
  props<{ contact: Contact }>()
);

export const contactRequestSuccessful = createAction(
  ContactActionEnum.ContactRequestSuccess
);

export const contactRequestFailed = createAction(
  ContactActionEnum.ContactRequestFailed
);
