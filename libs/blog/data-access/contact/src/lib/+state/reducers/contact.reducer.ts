import { createReducer, on } from '@ngrx/store';
import {
  doContactRequest,
  contactRequestSuccessful,
  contactRequestFailed,
} from '../actions/contact.actions';
import { ApiStateEnum, ContactState } from '../models/contact-state.model';

export const initialState: ContactState = {
  apiState: ApiStateEnum.Default,
};

export const contactReducers = createReducer(
  initialState,
  on(
    doContactRequest,
    (state: ContactState, action: ReturnType<typeof doContactRequest>) => {
      return {
        ...state,
        apiState: ApiStateEnum.Pending,
      };
    }
  ),
  on(
    contactRequestSuccessful,
    (
      state: ContactState,
      action: ReturnType<typeof contactRequestSuccessful>
    ) => {
      return {
        ...state,
        apiState: ApiStateEnum.Successful,
      };
    }
  ),
  on(
    contactRequestFailed,
    (state: ContactState, action: ReturnType<typeof contactRequestFailed>) => {
      return {
        ...state,
        apiState: ApiStateEnum.Error,
      };
    }
  )
);
