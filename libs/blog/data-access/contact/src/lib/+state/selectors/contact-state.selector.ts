import { createFeatureSelector } from '@ngrx/store';
import { contactFeatureKey, ContactState } from '../models/contact-state.model';

export const selectContactState =
  createFeatureSelector<ContactState>(contactFeatureKey);
