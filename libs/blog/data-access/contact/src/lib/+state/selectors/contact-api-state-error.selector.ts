import { createSelector } from '@ngrx/store';
import { ApiStateEnum } from '../models/contact-state.model';
import { selectContactApiState } from '@christianrothe-dev/blog/data-access/contact';

export const selectContactApiStateError = createSelector(
  selectContactApiState,
  (apiState: ApiStateEnum) => apiState === ApiStateEnum.Error
);
