import { createSelector } from '@ngrx/store';
import { ContactState } from '../models/contact-state.model';
import { selectContactState } from './contact-state.selector';

export const selectContactApiState = createSelector(
  selectContactState,
  (state: ContactState) => state.apiState
);
