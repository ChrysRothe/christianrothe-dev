import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY, exhaustMap, Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Action } from '@ngrx/store';
import { ContactClient } from '../../clients/contact.client';
import {
  ContactActionEnum,
  doContactRequest,
} from '../actions/contact.actions';

@Injectable()
export class ContactEffects {
  constructor(
    private actions$: Actions,
    private contactClient: ContactClient
  ) {}

  postContact$: Observable<Action> = createEffect(() => {
    return this.actions$.pipe(
      ofType(doContactRequest),
      exhaustMap((action: ReturnType<typeof doContactRequest>) =>
        this.contactClient.post(action.contact).pipe(
          map(() => ({
            type: ContactActionEnum.ContactRequestSuccess,
          })),
          catchError(() =>
            of({
              type: ContactActionEnum.ContactRequestFailed,
            })
          )
        )
      )
    );
  });
}
