export const contactFeatureKey = 'contact';

export interface ContactState {
	apiState: ApiStateEnum;
}

export interface ContactStateAware {
	[contactFeatureKey]: ContactState;
}

export enum ApiStateEnum {
	Pending = 'pending',
	Successful = 'successful',
	Error = 'error',
	Default = 'default',
}
