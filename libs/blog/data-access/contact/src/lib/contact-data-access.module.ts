import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactStateModule } from '@christianrothe-dev/blog/data-access/contact';
import { HttpClientModule } from '@angular/common/http';
import { ContactClient } from './clients/contact.client';

@NgModule({
  imports: [CommonModule, ContactStateModule, HttpClientModule],
  providers: [ContactClient],
})
export class ContactDataAccessModule {}
