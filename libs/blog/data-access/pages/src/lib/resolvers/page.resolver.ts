import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot,
} from '@angular/router';
import { catchError, Observable, of } from 'rxjs';
import { PageClient } from '../clients/page.client';
import { Page } from '../models/page.model';

@Injectable()
export class PageResolver implements Resolve<Page> {
  constructor(private pageClient: PageClient) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Page> | Promise<Page> | Page {
    const page = route.paramMap.get('page');

    if (null === page) {
      return this.nonPageContent;
    }

    return this.pageClient.get(page).pipe(
      catchError(() => {
        return this.nonPageContent;
      })
    );
  }

  private get nonPageContent(): Observable<Page> {
    return of({
      content: '',
    });
  }
}
