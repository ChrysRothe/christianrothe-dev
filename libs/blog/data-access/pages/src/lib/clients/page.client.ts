import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Page } from '../models/page.model';

@Injectable()
export class PageClient {
  constructor(private http: HttpClient) {}

  public get(page: string): Observable<Page> {
    return this.http.get<Page>(`api/pages/${page}`);
  }
}
