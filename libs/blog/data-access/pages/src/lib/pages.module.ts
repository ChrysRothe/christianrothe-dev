import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { PageClient } from './clients/page.client';
import { PageResolver } from './resolvers/page.resolver';

@NgModule({
  imports: [CommonModule, HttpClientModule],
  providers: [PageClient, PageResolver],
})
export class BlogDataAccessPagesModule {}
