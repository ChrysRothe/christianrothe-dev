export enum ResourceTypeEnum {
  Book = 'book',
  Podcast = 'podcast',
  Article = 'article',
  Video = 'video',
  Library = 'library',
}
