import {ResourceTypeEnum} from "../enums/resource-type.enum";

export interface Resource {
	name: string;
	url: string;
	type: ResourceTypeEnum;
}

export type Resources = Resource[];

