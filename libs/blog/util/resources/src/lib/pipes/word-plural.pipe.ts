import { NgModule, Pipe, PipeTransform } from '@angular/core';
import { CommonModule } from '@angular/common';

@Pipe({ name: 'wordPlural' })
export class WordPluralPipe implements PipeTransform {
  transform(value: string): string {
    switch (value.slice(-1)) {
      case 'y':
        return `${value.slice(0, -1)}ies`;
      default:
        return `${value}s`;
    }
  }
}

@NgModule({
  imports: [CommonModule],
  declarations: [WordPluralPipe],
  exports: [WordPluralPipe],
})
export class WordPluralPipeModule {}
