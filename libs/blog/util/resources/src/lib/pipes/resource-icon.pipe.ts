import { NgModule, Pipe, PipeTransform } from '@angular/core';
import { ResourceTypeEnum } from '@christianrothe-dev/blog/util/resources';
import { CommonModule } from '@angular/common';

const mapping: Record<ResourceTypeEnum, string> = {
  [ResourceTypeEnum.Podcast]: '🎤',
  [ResourceTypeEnum.Video]: '🎞',
  [ResourceTypeEnum.Library]: '🗳',
  [ResourceTypeEnum.Book]: '📖',
  [ResourceTypeEnum.Article]: '🗞',
};

@Pipe({ name: 'resourceIcon' })
export class ResourceIconPipe implements PipeTransform {
  transform(value: ResourceTypeEnum | string): string {
    return mapping[value as unknown as ResourceTypeEnum];
  }
}

@NgModule({
  imports: [CommonModule],
  declarations: [ResourceIconPipe],
  exports: [ResourceIconPipe],
})
export class ResourceIconPipeModule {}
