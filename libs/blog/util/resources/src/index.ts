export * from './lib/models/resource.model';
export * from './lib/enums/resource-type.enum';
export * from './lib/pipes/resource-icon.pipe';
export * from './lib/pipes/word-plural.pipe';
