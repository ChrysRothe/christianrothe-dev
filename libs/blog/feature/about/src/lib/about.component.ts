import { Component } from '@angular/core';

@Component({
  selector: 'about-page',
  template: `
    <ui-container [alignment]="'center'">
      <h1 uiH1>Christian Rothe</h1>
      <ui-sub-title>
        FullStack Developer, Angular Enthusiast and Coffee Lover. Currently
        working as Senior FullStack Developer at Facelift.
      </ui-sub-title>
      <ui-social-networks></ui-social-networks>
      <a href="mailto:christian.rothe.dev@gmail.com" uiButton> Get In Touch </a>
    </ui-container>
  `,
  styles: [
    `
      @import '~@christianrothe-dev/styles/colors';

      :host {
        display: flex;
        min-height: 100vh;
        justify-content: center;
        align-items: center;
      }

      ui-sub-title {
        text-align: center;
        margin-bottom: 1.5rem;
        width: 100%;
        max-width: 550px;
      }

      ui-social-networks {
        min-height: 70px;
      }
    `,
  ],
})
export class AboutComponent {}
