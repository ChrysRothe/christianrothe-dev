import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AboutComponent } from './about.component';
import {
  ButtonComponentModule,
  H1ComponentModule,
  SocialNetworksComponentModule,
  SubTitleComponentModule,
} from '@christianrothe-dev/blog/ui/elements';
import { ContainerComponentModule } from '@christianrothe-dev/blog/ui/layout';

@NgModule({
  declarations: [AboutComponent],
  imports: [
    CommonModule,
    ButtonComponentModule,
    ContainerComponentModule,
    SocialNetworksComponentModule,
    H1ComponentModule,
    SubTitleComponentModule,
    RouterModule.forChild([
      { path: '', pathMatch: 'full', component: AboutComponent },
    ]),
  ],
})
export class BlogFeatureAboutModule {}
