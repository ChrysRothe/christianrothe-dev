import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import {
  ColumnComponentModule,
  ColumnsComponentModule,
  ContainerComponentModule,
  SectionComponentModule,
} from '@christianrothe-dev/blog/ui/layout';
import {
  ButtonComponentModule,
  H1ComponentModule,
  H2ComponentModule,
} from '@christianrothe-dev/blog/ui/elements';
import { ResourceCardComponentModule } from '@christianrothe-dev/blog/ui/components';
import { ResourcesModule } from '@christianrothe-dev/blog/data-access/resources';

@NgModule({
  declarations: [HomeComponent],
  imports: [
    ButtonComponentModule,
    ResourceCardComponentModule,
    ResourcesModule,
    CommonModule,
    ContainerComponentModule,
    ColumnsComponentModule,
    ColumnComponentModule,
    SectionComponentModule,
    H1ComponentModule,
    H2ComponentModule,
    RouterModule.forChild([
      { path: '', pathMatch: 'full', component: HomeComponent },
    ]),
  ],
})
export class BlogFeatureHomeModule {}
