import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Resources } from '@christianrothe-dev/blog/util/resources';
import { selectLatestResources } from '@christianrothe-dev/blog/data-access/resources';

@Component({
  selector: 'home-page',
  template: `
    <section ui-section>
      <ui-container>
        <h1 uiH1>Awesome Angular Resources</h1>
        <p>
          Looking for awesome angular resources? Here you find a collection of
          podcasts, articles, books, videos and libraries including topics like
          architecture, state management, performance and a lot more.
        </p>
        <ui-columns *ngIf="resources$ | async as resources">
          <ui-column *ngFor="let resource of resources" size="is-fourth">
            <ui-resource-card [resource]="resource"></ui-resource-card>
          </ui-column>
        </ui-columns>
        <p>
          <a uiButton [routerLink]="'resources/angular'">
            Check out all resources
          </a>
        </p>
      </ui-container>
    </section>
  `,
  styles: [
    `
      @import '~@christianrothe-dev/styles/colors';

      p {
        margin-bottom: 2rem;
        font-size: 1.2rem;
        line-height: 1.7rem;
        width: 100%;
        max-width: 700px;

        &:last-child {
          margin-bottom: 0;
        }
      }

      ui-columns {
        margin-bottom: 0.75rem;
      }
    `,
  ],
})
export class HomeComponent {
  constructor(private store: Store) {}

  public resources$: Observable<Resources | null> = this.store.select(
    selectLatestResources(8)
  );
}
