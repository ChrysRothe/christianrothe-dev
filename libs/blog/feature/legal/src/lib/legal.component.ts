import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Data } from '@angular/router';
import { map, Observable } from 'rxjs';

@Component({
  selector: 'legal-page',
  template: `
    <section ui-section>
      <ui-container>
        <div [innerHTML]="content | async"></div>
      </ui-container>
    </section>
  `,
  styles: [
    `
      @import '~@christianrothe-dev/styles/colors';

      ::ng-deep {
        h1 {
          font-size: 2rem;
          margin-bottom: 1rem;
          font-weight: 700;
        }

        h2 {
          font-size: 1.4rem;
          margin: 2rem 0 1rem;
          font-weight: 700;
        }

        h3 {
          font-size: 1.1rem;
          font-weight: 700;
          margin-bottom: 0.75rem;
        }

        h4 {
          font-weight: 700;
          margin-bottom: 0.75rem;
        }

        p {
          margin-bottom: 1rem;
        }
      }
    `,
  ],
})
export class LegalComponent implements OnInit {
  public content?: Observable<string>;

  constructor(private route: ActivatedRoute) {}

  public ngOnInit(): void {
    this.content = this.route.data.pipe(
      map((data: Data) => {
        return data['page'].content;
      })
    );
  }
}
