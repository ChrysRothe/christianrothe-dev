import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { LegalComponent } from './legal.component';
import {
  ContainerComponentModule,
  SectionComponentModule,
} from '@christianrothe-dev/blog/ui/layout';
import { H1ComponentModule } from '@christianrothe-dev/blog/ui/elements';
import { HttpClientModule } from '@angular/common/http';
import {
  BlogDataAccessPagesModule,
  PageResolver,
} from '@christianrothe-dev/blog/data-access/pages';

@NgModule({
  declarations: [LegalComponent],
  imports: [
    HttpClientModule,
    H1ComponentModule,
    ContainerComponentModule,
    CommonModule,
    BlogDataAccessPagesModule,
    SectionComponentModule,
    RouterModule.forChild([
      {
        path: ':page',
        pathMatch: 'full',
        component: LegalComponent,
        resolve: {
          page: PageResolver,
        },
      },
      {
        path: '',
        redirectTo: 'imprint',
      },
    ]),
  ],
  providers: [PageResolver],
})
export class BlogFeatureLegalModule {}
