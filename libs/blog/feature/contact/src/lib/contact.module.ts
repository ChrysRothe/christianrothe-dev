import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ContactComponent } from './contact.component';
import {
  ColumnComponentModule,
  ColumnsComponentModule,
  ContainerComponentModule,
} from '@christianrothe-dev/blog/ui/layout';
import { SectionComponentModule } from '@christianrothe-dev/blog/ui/layout';
import { ContactLinksComponentModule } from './contact-links/contact-links.component';
import { FormComponentModule } from './form/form.component';

@NgModule({
  declarations: [ContactComponent],
  imports: [
    CommonModule,
    ContainerComponentModule,
    ContactLinksComponentModule,
    ColumnComponentModule,
    ColumnsComponentModule,
    FormComponentModule,
    SectionComponentModule,
    RouterModule.forChild([
      { path: '', pathMatch: 'full', component: ContactComponent },
    ]),
  ],
})
export class BlogFeatureContactModule {}
