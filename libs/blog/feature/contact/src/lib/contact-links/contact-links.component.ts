import { ChangeDetectionStrategy, Component, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'contact-contact-links',
  template: `
    <ul>
      <li>
        <a
          href="https://www.linkedin.com/in/christian-rothe-dev"
          target="_blank"
          rel="noopener"
        >
          <i class="fab fa-linkedin"></i> LinkedIn
        </a>
      </li>
      <li>
        <a href="https://twitter.com/ChrysRothe" target="_blank" rel="noopener">
          <i class="fab fa-twitter"></i> Twitter
        </a>
      </li>
      <li>
        <a href="https://github.com/chrysrothe" target="_blank" rel="noopener">
          <i class="fab fa-github-square"></i> Github
        </a>
      </li>
      <li>
        <a href="https://gitlab.com/ChrysRothe" target="_blank" rel="noopener">
          <i class="fab fa-gitlab"></i> Gitlab
        </a>
      </li>
    </ul>
  `,
  styles: [
    `
      @import '~@christianrothe-dev/styles/colors';

      :host {
        display: block;
      }

      li {
        margin-bottom: 1rem;
        transition: transform 150ms ease-out, color 350ms ease-out;
        color: $color-text-secondary;
        font-size: 1.1rem;

        &:hover {
          transform: translateY(-1px);
          color: $color-text-primary;
        }
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContactLinksComponent {}

@NgModule({
  imports: [CommonModule],
  declarations: [ContactLinksComponent],
  exports: [ContactLinksComponent],
})
export class ContactLinksComponentModule {}
