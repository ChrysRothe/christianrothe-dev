import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'contact-page',
  template: `
    <section ui-section>
      <ui-container>
        <h1>Get In Touch</h1>
        <ui-columns>
          <ui-column [size]="'is-two-third'">
            <contact-form></contact-form>
          </ui-column>
          <ui-column>
            <contact-contact-links></contact-contact-links>
          </ui-column>
        </ui-columns>
      </ui-container>
    </section>
  `,
  styles: [
    `
      @import '~@christianrothe-dev/styles/colors';

      :host {
        min-height: 100vh;
        display: block;
      }

      h1 {
        font-weight: 700;
        color: $color-text-primary;
        margin-bottom: 1.5rem;
        font-size: 2rem;
      }

      contact-contact-links {
        padding-top: 1.3rem;
      }

      contact-form {
        display: block;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContactComponent {}
