import { Injectable } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';

@Injectable()
export class ContactFormBuilder {
  constructor(private formBuilder: FormBuilder) {}

  public build(): FormGroup {
    return this.formBuilder.group({
      name: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email]),
      message: new FormControl('', [
        Validators.required,
        Validators.minLength(10),
      ]),
      privacy: new FormControl(false, Validators.requiredTrue),
    });
  }
}
