import { ChangeDetectionStrategy, Component, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  DisableControlDirectiveModule,
  FormElementComponentModule,
  InputComponentModule,
  TextareaComponentModule,
} from '@christianrothe-dev/blog/ui/forms';
import { FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  AlertComponentModule,
  ButtonComponentModule,
} from '@christianrothe-dev/blog/ui/elements';
import { Store } from '@ngrx/store';
import {
  ContactActionEnum,
  ContactDataAccessModule,
  selectContactApiPendingOrSuccessful,
  selectContactApiStateError,
  selectContactApiStatePending,
  selectContactApiStateSuccessful,
} from '@christianrothe-dev/blog/data-access/contact';
import { Observable } from 'rxjs';
import { ContactFormBuilder } from './contact-form.builder';

@Component({
  selector: 'contact-form',
  template: `
    <form [formGroup]="form" (ngSubmit)="onSubmit()">
      <form-element [label]="'Your Name'">
        <input
          [uiFormsDisableControl]="isPendingOrSuccessful$ | async"
          formControlName="name"
          uiFormsInput
          type="text"
          placeholder="e.g. Max Mustermann"
        />
      </form-element>
      <form-element [label]="'Your Email'">
        <input
          [uiFormsDisableControl]="isPendingOrSuccessful$ | async"
          formControlName="email"
          uiFormsInput
          type="email"
          placeholder="e.g. max.mustermann@email.com"
        />
      </form-element>
      <form-element [label]="'Your Message'">
        <textarea
          [uiFormsDisableControl]="isPendingOrSuccessful$ | async"
          formControlName="message"
          uiFormsTextarea
        ></textarea>
      </form-element>
      <form-element>
        <input
          [uiFormsDisableControl]="isPendingOrSuccessful$ | async"
          formControlName="privacy"
          type="checkbox"
        />
        I accept the
        <a href="">privacy policy</a>
      </form-element>
      <button
        uiButton
        [disabled]="form.invalid || (isPendingOrSuccessful$ | async)"
        [isPending]="isPending$ | async"
      >
        Send
      </button>
    </form>
    <ui-alert *ngIf="isSuccessful$ | async" [type]="'success'">
      Thanks a lot. I will get in touch with you as soon possible.
    </ui-alert>
    <ui-alert *ngIf="isError$ | async" [type]="'error'">
      Something went wrong. Please try another time.
    </ui-alert>
  `,
  styles: [
    `
      form {
        margin-bottom: 1rem;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormComponent {
  constructor(
    private contactFormBuilder: ContactFormBuilder,
    private store: Store
  ) {
    this.form = this.contactFormBuilder.build();
  }

  public form: FormGroup;

  public isPendingOrSuccessful$: Observable<boolean> =
    this.store.select<boolean>(selectContactApiPendingOrSuccessful);

  public isPending$: Observable<boolean> = this.store.select<boolean>(
    selectContactApiStatePending
  );

  public isSuccessful$: Observable<boolean> = this.store.select<boolean>(
    selectContactApiStateSuccessful
  );

  public isError$: Observable<boolean> = this.store.select<boolean>(
    selectContactApiStateError
  );

  public onSubmit(): void {
    if (this.form.invalid) {
      return;
    }

    this.store.dispatch({
      type: ContactActionEnum.ContactRequest,
      contact: this.form.value,
    });
  }
}

@NgModule({
  imports: [
    AlertComponentModule,
    ButtonComponentModule,
    CommonModule,
    ContactDataAccessModule,
    DisableControlDirectiveModule,
    InputComponentModule,
    FormElementComponentModule,
    ReactiveFormsModule,
    FormsModule,
    TextareaComponentModule,
  ],
  declarations: [FormComponent],
  exports: [FormComponent],
  providers: [ContactFormBuilder],
})
export class FormComponentModule {}
