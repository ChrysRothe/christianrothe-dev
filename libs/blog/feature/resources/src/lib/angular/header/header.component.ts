import { Component, NgModule, ChangeDetectionStrategy } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  ColumnComponentModule,
  ColumnsComponent,
  ColumnsComponentModule,
  ContainerComponentModule,
} from '@christianrothe-dev/blog/ui/layout';
import {
  H1ComponentModule,
  SubTitleComponentModule,
} from '@christianrothe-dev/blog/ui/elements';
import {
  ResourceIconPipeModule,
  ResourceTypeEnum,
  WordPluralPipeModule,
} from '@christianrothe-dev/blog/util/resources';

@Component({
  selector: 'resources-header',
  template: `
    <ui-container>
      <ui-columns>
        <ui-column size="is-half">
          <h1 uiH1>Find awesome Angular Resources</h1>
          <ui-sub-title>
            <span *ngFor="let type of resourceTypes">
              {{ type | resourceIcon }} {{ type | wordPlural | titlecase }}
            </span>
          </ui-sub-title>
          <p>
            Looking for awesome angular resources to level up your angular
            knowledge? Here you find a collection of podcasts, articles, books,
            videos and libraries covering important topics like: architecture,
            monorepo strategy, state management, performance, search engine
            optimization, server side rendering and many more.
          </p>
        </ui-column>
      </ui-columns>
    </ui-container>
  `,
  styles: [
    `
      @import '~@christianrothe-dev/styles/colors';

      :host {
        display: block;
      }

      ui-sub-title {
        margin-bottom: 2rem;
      }

      p {
        margin-bottom: 1rem;
        font-size: 1.2rem;
        line-height: 1.7rem;
      }

      span {
        display: inline-block;
        padding: 0 0.75rem;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent {
  public resourceTypes: string[] = Object.values(ResourceTypeEnum);
}

@NgModule({
  imports: [
    CommonModule,
    ContainerComponentModule,
    ColumnsComponentModule,
    ColumnComponentModule,
    H1ComponentModule,
    SubTitleComponentModule,
    ResourceIconPipeModule,
    WordPluralPipeModule,
  ],
  declarations: [HeaderComponent],
  exports: [HeaderComponent],
})
export class HeaderComponentModule {}
