import { Component, NgModule, ChangeDetectionStrategy } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  ColumnComponentModule,
  ColumnsComponentModule,
  ContainerComponentModule,
  SectionComponentModule,
} from '@christianrothe-dev/blog/ui/layout';
import {
  ResourcesModule,
  selectFilteredResources,
} from '@christianrothe-dev/blog/data-access/resources';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { HeaderComponentModule } from './header/header.component';
import { SearchComponentModule } from './search/search.component';
import { Resources } from '@christianrothe-dev/blog/util/resources';
import { ResourceCardComponentModule } from '@christianrothe-dev/blog/ui/components';

@Component({
  selector: 'resources-angular',
  template: `
    <section ui-section>
      <resources-header></resources-header>
      <ui-container>
        <resources-search></resources-search>
        <ui-columns *ngIf="resources$ | async as resources; else noResults">
          <ui-column *ngFor="let resource of resources" size="is-fourth">
            <ui-resource-card [resource]="resource"></ui-resource-card>
          </ui-column>
        </ui-columns>
        <ng-template #noResults>No Results found</ng-template>
      </ui-container>
    </section>
  `,
  styles: [
    `
      resources-header {
        margin-bottom: 2rem;
      }

      resources-search {
        margin-bottom: 1rem;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AngularComponent {
  constructor(private store: Store) {}

  public resources$: Observable<Resources | null> = this.store.select(
    selectFilteredResources
  );
}

@NgModule({
  imports: [
    CommonModule,
    ResourceCardComponentModule,
    ResourcesModule,
    ColumnsComponentModule,
    ColumnComponentModule,
    ContainerComponentModule,
    HeaderComponentModule,
    SectionComponentModule,
    SearchComponentModule,
  ],
  declarations: [AngularComponent],
  exports: [AngularComponent],
})
export class AngularComponentModule {}
