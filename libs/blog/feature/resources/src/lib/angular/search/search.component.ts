import {
  ChangeDetectionStrategy,
  Component,
  NgModule,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  InputComponentModule,
  SelectComponentModule,
} from '@christianrothe-dev/blog/ui/forms';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { debounceTime, Observable, Subject, takeUntil } from 'rxjs';
import {
  angularResourcesFeature,
  ResourceActionEnum,
} from '@christianrothe-dev/blog/data-access/resources';
import { Store } from '@ngrx/store';
import {
  ResourceIconPipeModule,
  ResourceTypeEnum,
  WordPluralPipeModule,
} from '@christianrothe-dev/blog/util/resources';
import {
  ResourceFilter,
  ResourceFilterKeyEnum,
} from '@christianrothe-dev/blog/data-access/resources';

@Component({
  selector: 'resources-search',
  template: `
    <form [formGroup]="form">
      <select formControlName="type" uiFormsSelect>
        <option value="all">🟢 All</option>
        <option *ngFor="let type of resourceTypes" [value]="type">
          {{ type | resourceIcon }} {{ type | wordPlural | titlecase }}
        </option>
      </select>
      <input
        uiFormsInput
        formControlName="search"
        placeholder="Search for Topic"
        type="text"
      />
    </form>
  `,
  styles: [
    `
      :host {
        display: block;
      }

      form {
        display: flex;
        gap: 1rem;
      }

      select {
        width: 150px;
        flex-shrink: 0;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchComponent implements OnInit, OnDestroy {
  private destroy$: Subject<void> = new Subject<void>();

  public form: FormGroup = new FormGroup({
    [ResourceFilterKeyEnum.Search]: new FormControl(''),
    [ResourceFilterKeyEnum.Type]: new FormControl('all'),
  });

  public filter$: Observable<ResourceFilter> = this.store.select(
    angularResourcesFeature.selectFilter
  );

  public resourceTypes: string[] = Object.values(ResourceTypeEnum);

  constructor(private store: Store) {}

  public ngOnInit(): void {
    this.form.valueChanges
      .pipe(debounceTime(100), takeUntil(this.destroy$))
      .subscribe((filter: ResourceFilter) => {
        this.store.dispatch({
          type: ResourceActionEnum.UpdateResourceFilter,
          filter,
        });
      });

    this.filter$
      .pipe(takeUntil(this.destroy$))
      .subscribe((filter: ResourceFilter) => {
        this.form.patchValue(filter, { emitEvent: false });
      });
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}

@NgModule({
  imports: [
    CommonModule,
    InputComponentModule,
    ReactiveFormsModule,
    SelectComponentModule,
    ResourceIconPipeModule,
    WordPluralPipeModule,
  ],
  declarations: [SearchComponent],
  exports: [SearchComponent],
})
export class SearchComponentModule {}
