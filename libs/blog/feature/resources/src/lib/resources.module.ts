import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {
  AngularComponent,
  AngularComponentModule,
} from './angular/angular.component';

@NgModule({
  imports: [
    AngularComponentModule,
    CommonModule,
    RouterModule.forChild([
      {
        path: 'angular',
        pathMatch: 'full',
        component: AngularComponent,
        data: {
          title: 'Awesome Angular Resources',
        },
      },
    ]),
  ],
})
export class BlogFeatureResourcesModule {}
